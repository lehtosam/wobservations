import React, { Component } from 'react';
import { Card, Avatar, Tag } from 'antd';
import './Location.css';

class Location extends Component {
    constructor(props) {
        super(props);
        this.locationName = props.locationName;
        this.locationId = props.locationId;
        this.locationLatitude = props.locationLatitude;
        this.locationLongitude = props.locationLongitude;
        this.locationDegreesMax = props.locationDegreesMax;
        this.locationDegreesMin = props.locationDegreesMin;
        this.locationDegreesAvg = props.locationDegreesAvg;
        this.locationFlagUrl = props.locationFlagUrl;     
        this.handleSelectLocation = this.handleSelectLocation.bind(this);
    }

    handleSelectLocation(locationId,locationName,locationLatitude,locationLongitude){
        this.props.selectLocation(locationId,locationName,locationLatitude,locationLongitude);
    }

    render(props) {
        return(
            <Card
                onClick={() => this.handleSelectLocation(this.locationId,this.locationName,this.locationLatitude,this.locationLongitude)}
                style={{width: 275, backgroundColor: (this.props.locationSelected? 'rgba(0, 0, 0, 0.15)' : 'rgba(0, 0, 0, 0.05)')}}
                hoverable={true}
                actions={[
                    <Tag color="#EF476F">{"Max: " + Math.round(this.props.locationDegreesMax * 100)/100}</Tag>,
                    <Tag color="#06D6A0">{"Avg: " + Math.round(this.props.locationDegreesAvg * 100)/100}</Tag>,
                    <Tag color="#118AB2">{"Min: " + Math.round(this.props.locationDegreesMin * 100)/100}</Tag>
                ]}
            >
                <Card.Meta
                    avatar={<Avatar src={this.locationFlagUrl}/>}
                    title={this.locationName}
                />
            </Card>
        )
    }
}

export default Location;