import React, { Component } from 'react';
import { List, Avatar, Icon } from 'antd';
import './Observation.css';

class Observation extends Component {
    constructor(props) {
        super(props);
        this.observationId = props.observationId;
        this.observationTimestamp = props.observationTimestamp;
        this.observationLocationId = props.observationLocationId;
        this.observationDegrees = props.observationDegrees;
        this.handleRemoveObservation = this.handleRemoveObservation.bind(this);
    }

    handleRemoveObservation(observationId, observationLocationId){
        this.props.removeObservation(observationId, observationLocationId);
    }

    render(props) {
        return(
            <List.Item actions={[<Icon type="close-circle-o" style={{ fontSize: 18, color: '#FFD166'}} onClick={() => this.handleRemoveObservation(this.observationId, this.observationLocationId)}/>]}
            style={{textAlign: 'left'}}
            >
                <List.Item.Meta
                    avatar={<Avatar 
                        icon={(this.observationDegrees > 0 ? "plus" : "minus")} 
                        style={{backgroundColor: (this.observationDegrees > 0 ? '#D8315B' : '#3E92CC')}}
                        size="default"/>}
                    title={this.observationDegrees + "\u2103"}
                    description=
                        {new Intl.DateTimeFormat('en-GB', { 
                            year: 'numeric', 
                            month: 'long', 
                            day: '2-digit',
                            hour: 'numeric',
                            minute: 'numeric',
                            second: 'numeric'
                          }).format(new Date(this.observationTimestamp))}
                />
            </List.Item>
        )
    }
}

export default Observation;