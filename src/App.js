import React, { Component } from "react";
import Location from "./Location/Location";
import Observation from "./Observation/Observation";
import ObservationForm from "./ObservationForm/ObservationForm";
import PigeonMap from "./Map/PigeonMap";
import { DB_CONFIG } from "./Config/config";
import firebase from "firebase/app";
import { List, Layout, Popover, Card, Tag } from "antd";
import Loading from "react-simple-loading";
import "firebase/database";
import "./App.css";

const { Header, Content } = Layout;

function isMatching(locationId) {
  return function(item) {
    return item.observationLocationId.match(locationId);
  };
}

function fitsIntoLastDay() {
  return function(observation) {
    var observationtimeStamp =
      new Date(observation.observationTimestamp).getTime() / 1000;
    var timeStamp = Math.round(new Date().getTime() / 1000);
    var timeStampYesterday = timeStamp - 24 * 3600;
    var is24 = observationtimeStamp >= new Date(timeStampYesterday).getTime();
    return is24;
  };
}

class App extends Component {
  constructor(props) {
    super(props);
    this.selectLocation = this.selectLocation.bind(this);
    this.addObservation = this.addObservation.bind(this);
    this.removeObservation = this.removeObservation.bind(this);
    this.updateLocationValues = this.updateLocationValues.bind(this);
    this.getFirstLocationAndSetSelected = this.getFirstLocationAndSetSelected.bind(
      this
    );

    this.app = firebase.initializeApp(DB_CONFIG);
    this.locationsRef = this.app
      .database()
      .ref()
      .child("locations");

    this.observationsRef = this.app
      .database()
      .ref()
      .child("observations");

    this.state = {
      locations: [],
      observations: [],
      selectedLocationId: "",
      selectedLocationName: "",
      selectedLocationCoordinates: []
    };
  }

  componentDidMount() {
    const previousObservations = this.state.observations;

    //Data snapshot
    this.locationsRef.on("child_added", snap => {
      const previousLocations = this.state.locations.slice();
      previousLocations.push({
        id: snap.key,
        locationName: snap.val().locationName,
        locationLatitude: snap.val().locationLatitude,
        locationLongitude: snap.val().locationLongitude,
        locationDegreesMax: snap.val().maxDegrees,
        locationDegreesMin: snap.val().minDegrees,
        locationDegreesAvg: snap.val().avgDegrees,
        locationFlagUrl: snap.val().flagUrl
      });
      this.setState(
        {
          locations: previousLocations
        },
        //Set the selected location parameters once the locations list has been set
        () => {
          if (this.state.selectedLocationId.length === 0) {
            this.getFirstLocationAndSetSelected();
          }
        }
      );
    });

    this.locationsRef.on("child_removed", snap => {
      const previousLocations = [];
      for (var i = 0; i < previousLocations.length; i++) {
        if (previousLocations[i].id === snap.key) {
          previousLocations.splice(i, 1);
        }
      }

      this.setState({
        locations: previousLocations
      });
    });

    this.observationsRef.on("child_added", snap => {
      previousObservations.push({
        id: snap.key,
        observationTimestamp: snap.val().observationTimestamp,
        observationLocationId: snap.val().observationLocationId,
        observationDegrees: snap.val().observationDegrees
      });

      this.setState({
        observations: previousObservations
      });

      //Update the location values
      this.updateLocationValues(snap.val().observationLocationId);
    });

    this.observationsRef.on("child_removed", snap => {
      for (var i = 0; i < previousObservations.length; i++) {
        if (previousObservations[i].id === snap.key) {
          previousObservations.splice(i, 1);
        }
      }

      this.setState({
        observations: previousObservations
      });

      //Update the location values
      this.updateLocationValues(snap.val().observationLocationId);
    });
  }

  addObservation(
    observationTimestamp,
    observationDegrees,
    observationLocationId
  ) {
    this.observationsRef.push().set({
      observationTimestamp: observationTimestamp,
      observationDegrees: observationDegrees,
      observationLocationId: observationLocationId
    });
  }

  removeObservation(observationId, observationLocationId) {
    this.observationsRef.child(observationId).remove();
  }

  selectLocation(
    locationId,
    locationName,
    locationLatitude,
    locationLongitude
  ) {
    var coordinates = [locationLatitude, locationLongitude];

    this.setState({
      selectedLocationId: locationId,
      selectedLocationName: locationName,
      selectedLocationCoordinates: coordinates
    });
  }

  //Update min, max and average for location
  updateLocationValues(locationId) {
    if (locationId) {
      const degrees = this.state.observations
        .filter(isMatching(locationId))
        .map(a => a.observationDegrees);

      if (degrees.length > 0) {
        const max = Math.max(...degrees);
        const min = Math.min(...degrees);
        const avg = degrees.reduce((a, b) => a + b, 0) / degrees.length;

        const updates = {};
        updates["/" + locationId + "/maxDegrees"] = max;
        updates["/" + locationId + "/minDegrees"] = min;
        updates["/" + locationId + "/avgDegrees"] = avg;

        this.locationsRef.update(updates);

        //Unholy way to do this, but works for now
        var updateLocations = this.state.locations;

        const locationIndex = updateLocations.findIndex(
          location => location.id === locationId
        );

        updateLocations[locationIndex].locationDegreesMax = max;
        updateLocations[locationIndex].locationDegreesMin = min;
        updateLocations[locationIndex].locationDegreesAvg = avg;

        this.setState({
          locations: updateLocations
        });
      }
    }
  }

  //Get first location from the established list and set the selected location's parameters
  getFirstLocationAndSetSelected() {
    var code = "";
    var name = "";
    var coordinates = [];

    //Make sure we have locations to work with
    if (this.state.locations.length > 0) {
      var firstLocation = this.state.locations[0];
      code = firstLocation.id;
      name = firstLocation.locationName;
      coordinates = [];
      coordinates.push(
        firstLocation.locationLatitude,
        firstLocation.locationLongitude
      );
    }

    this.setState({
      selectedLocationId: code,
      selectedLocationName: name,
      selectedLocationCoordinates: coordinates
    });
  }

  render() {
    const infoContent = (
      <div>
        <p>
          This is a simple web app for all your temperature recording needs.
        </p>
        <p>
          Begin by selecting the desired location from the left side and move on
          to recording temperatures on the right-hand list. The existing
          observations can also be deleted.
        </p>
        <p>
          The Maximum (Max), Average (Avg) and Minimum (Min) figures are updated
          in the locations list as they are added or removed.
        </p>
        <p>
          This app was built by using the following:
          <ul>
            <li>reactJS</li>
            <li>create-react-app</li>
            <li>react-simple-loading</li>
            <li>Google Firebase</li>
            <ul>
              <li>real time database</li>
              <li>storage</li>
              <li>hosting</li>
            </ul>
            <li>pigeonmap for map and markers</li>
            <li>react-window-size to fit the map to viewport</li>
            <li>Ant design UI elements for design</li>
            <li>Colours from coolors.co</li>
            <li>
              <div>
                Flag icons made by{" "}
                <a href="http://www.freepik.com" title="Freepik">
                  Freepik
                </a>{" "}
                from{" "}
                <a href="https://www.flaticon.com/" title="Flaticon">
                  www.flaticon.com
                </a>{" "}
                is licensed by{" "}
                <a
                  href="http://creativecommons.org/licenses/by/3.0/"
                  title="Creative Commons BY 3.0"
                >
                  CC 3.0 BY
                </a>
              </div>
            </li>
          </ul>
        </p>
      </div>
    );

    const degreesForSelectedLocation = this.state.observations
      .filter(isMatching(this.state.selectedLocationId))
      .reverse()
      .map(a => a.observationDegrees);

    const degreesForSelectedLocationAndLastDay = this.state.observations
      .filter(isMatching(this.state.selectedLocationId))
      .filter(fitsIntoLastDay())
      .reverse()
      .map(a => a.observationDegrees);

    console.log({ degreesForSelectedLocationAndLastDay });

    //Just to make sure all the needed props are there
    if (this.state.selectedLocationId.length === 0) {
      console.log("Wait a moment");
      return (
        <div>
          <Loading />
        </div>
      );
    }
    return (
      <Layout className="App">
        <Header className="App-header">
          <Popover content={infoContent} title="Info" trigger="click">
            <h1>Wobservations (click me)</h1>
          </Popover>
        </Header>
        <Content className="App-content">
          <PigeonMap
            center={this.state.selectedLocationCoordinates}
            locations={this.state.locations}
          />
          <List className="App-locations">
            <div>
              <h3 style={{ marginBottom: 16, marginTop: 16 }}>Locations</h3>
              <List
                bordered
                dataSource={this.state.locations}
                renderItem={location => (
                  <List.Item>
                    <Location
                      locationName={location.locationName}
                      locationId={location.id}
                      locationLatitude={location.locationLatitude}
                      locationLongitude={location.locationLongitude}
                      locationDegreesMax={location.locationDegreesMax}
                      locationDegreesMin={location.locationDegreesMin}
                      locationDegreesAvg={location.locationDegreesAvg}
                      key={location.id}
                      locationFlagUrl={location.locationFlagUrl}
                      selectLocation={this.selectLocation}
                      locationSelected={
                        this.state.selectedLocationId === location.id
                      }
                    />
                  </List.Item>
                )}
              />
            </div>
          </List>
          <List className="App-observations">
            <div>
              <h3 style={{ marginBottom: 16, marginTop: 16 }}>
                Observations for {this.state.selectedLocationName}
              </h3>
              <List
                header={
                  <div className="observationFormContainer">
                    <Card
                      style={{ width: 407, align: "center" }}
                      actions={[
                        <Tag color="#EF476F">
                          {"24H High: " +
                            (degreesForSelectedLocationAndLastDay.length > 0
                              ? Math.round(
                                  Math.max(
                                    ...degreesForSelectedLocationAndLastDay
                                  ) * 100
                                ) / 100
                              : "-")}
                        </Tag>,
                        <Tag color="#06D6A0">
                          {"Recent: " +
                            Math.round(degreesForSelectedLocation[0] * 100) /
                              100}
                        </Tag>,
                        <Tag color="#118AB2">
                          {"24H Low: " +
                            (degreesForSelectedLocationAndLastDay.length > 0
                              ? Math.round(
                                  Math.min(
                                    ...degreesForSelectedLocationAndLastDay
                                  ) * 100
                                ) / 100
                              : "-")}
                        </Tag>
                      ]}
                    >
                      <Card.Meta
                        title={
                          <ObservationForm
                            addObservation={this.addObservation}
                            selectedLocationFromParent={
                              this.state.selectedLocationId
                            }
                          />
                        }
                      />
                    </Card>
                  </div>
                }
                style={{ backgroundcolor: "FFFAFF" }}
                size="small"
                bordered
                dataSource={this.state.observations
                  .filter(isMatching(this.state.selectedLocationId))
                  .reverse()}
                renderItem={observation => (
                  <Observation
                    observationId={observation.id}
                    observationTimestamp={observation.observationTimestamp}
                    observationLocationId={observation.observationLocationId}
                    observationDegrees={observation.observationDegrees}
                    key={observation.id}
                    removeObservation={this.removeObservation}
                  />
                )}
              />
            </div>
          </List>
        </Content>
      </Layout>
    );
  }
}

export default App;
