import React, { Component } from 'react';
import Map from 'pigeon-maps';
import Marker from 'pigeon-marker';
import windowSize from 'react-window-size';
import { MAPBOX_ACCESS_TOKEN } from "../Config/config";

const mapbox = (mapboxId, accessToken) => (x, y, z) => {
  const retina = typeof window !== 'undefined' && window.devicePixelRatio >= 2 ? '@2x' : ''
  return `https://api.mapbox.com/styles/v1/mapbox/${mapboxId}/tiles/256/${z}/${x}/${y}${retina}?access_token=${accessToken}`
}

const providers = {
    osm: (x, y, z) => {
      const s = String.fromCharCode(97 + (x + y + z) % 3)
      return `https://${s}.tile.openstreetmap.org/${z}/${x}/${y}.png`
    },
    wikimedia: (x, y, z) => {
      const retina = typeof window !== 'undefined' && window.devicePixelRatio >= 2 ? '@2x' : ''
      return `https://maps.wikimedia.org/osm-intl/${z}/${x}/${y}${retina}.png`
    },
    stamen: (x, y, z) => {
      const retina = typeof window !== 'undefined' && window.devicePixelRatio >= 2 ? '@2x' : ''
      return `https://stamen-tiles.a.ssl.fastly.net/terrain/${z}/${x}/${y}${retina}.jpg`
    },
    streets: mapbox('streets-v10', MAPBOX_ACCESS_TOKEN),
    satellite: mapbox('satellite-streets-v10', MAPBOX_ACCESS_TOKEN),
    outdoors: mapbox('outdoors-v10', MAPBOX_ACCESS_TOKEN),
    light: mapbox('light-v9', MAPBOX_ACCESS_TOKEN),
    dark: mapbox('dark-v9', MAPBOX_ACCESS_TOKEN)
}

class PigeonMap extends Component {
    constructor (props) {
      super(props);

      this.state = {
        zoom: 13,
        provider: 'light',
        zoomOnMouseWheel: true
      }
    }
  
    zoomIn = () => {
      this.setState({
        zoom: Math.min(this.state.zoom + 1, 18)
      })
    }
  
    zoomOut = () => {
      this.setState({
        zoom: Math.max(this.state.zoom - 1, 1)
      })
    }
  
    handleBoundsChange = ({ center, zoom, bounds, initial }) => {
      if (initial) {
        console.log('Got initial bounds: ', bounds)
      }
      this.setState({ center, zoom })
    }
  
    handleClick = ({ event, latLng, pixel }) => {
      console.log('Map clicked!', latLng, pixel)
    }
  
    handleMarkerClick = ({ event, payload, anchor }) => {
      console.log(`Marker #${payload} clicked at: `, anchor)
    }
  
    render () {
      const { zoom, provider, zoomOnMouseWheel } = this.state;
      const marginFromTop = 50;
      //Setup markers from locations
      const markers = this.props.locations.map((location,index) => 
        <Marker anchor={[location.locationLatitude, location.locationLongitude]} payload={index} onClick={this.handleMarkerClick} />
      );
      return (
        <div style={{textAlign: 'center', marginFromTop}}>
          <Map center={this.props.center}
               zoom={zoom}
               provider={providers[provider]}
               onBoundsChanged={this.handleBoundsChange}
               onClick={this.handleClick}
               zoomOnMouseWheel={zoomOnMouseWheel}
               width={this.props.windowWidth}
               height={this.props.windowHeight-marginFromTop}>
            {markers}
          </Map>
        </div>
      )
    }
  }

export default windowSize(PigeonMap);