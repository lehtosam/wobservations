import React, { Component } from 'react';
import { Form, Button, DatePicker, InputNumber} from 'antd';
import './ObservationForm.css';

class ObservationForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            newObservationTimestamp: '',
            newObservationDegrees: ''
        };

        this.handleUserInputForDegrees = this.handleUserInputForDegrees.bind(this);
        this.handleUserInputForDateTime = this.handleUserInputForDateTime.bind(this);
        this.writeObservation = this.writeObservation.bind(this);
    }
   
    handleUserInputForDateTime(e) {
        this.setState({
            newObservationTimestamp: e, //Value from the user
        })
    }
    
    handleUserInputForDegrees(e) {
        this.setState({
            newObservationDegrees: e, //Value from the user
        })
    }

    writeObservation() {        
        const timestamp = JSON.parse(JSON.stringify(this.state.newObservationTimestamp));
        const degrees = parseFloat(this.state.newObservationDegrees);

        this.props.addObservation(timestamp, degrees, this.props.selectedLocationFromParent);        

        this.setState({
            newObservationTimestamp: '',
            newObservationDegrees: ''
        })
    }

    render(){
        return (
            <div className="observationFormContainer">
                <Form layout="inline">
                    <Form.Item>
                        <DatePicker
                            value={this.state.newObservationTimestamp} //TODO: Fix this, shouldn't throw an error, how to clear the selected date after saving?
                            showTime
                            format="YYYY-MM-DD HH:mm:ss"
                            placeholder="Select Time"
                            onChange={this.handleUserInputForDateTime}
                            onOk={this.handleUserInputForDateTime}
                        />
                    </Form.Item>
                    <Form.Item>
                        <InputNumber
                            value={this.state.newObservationDegrees}
                            min={-100}
                            max={100}
                            formatter={value => `${value}\u00b0`}
                            parser={value => value.replace('\u00b0', '')}
                            onChange={this.handleUserInputForDegrees}
                        />
                    </Form.Item>
                    <Form.Item className="buttonWithoutMargin">
                        <Button type="default" onClick={this.writeObservation}>Save</Button>
                    </Form.Item>
                </Form>
            </div>
        )
    }
}

export default ObservationForm;