# Wobservations

This project aims to provide a simple easy-to-use web platform for weather observations (mainly temperature observations) in selected locations.

## Dependencies

The project is written with react, bootstrapped with create-react-app, uses Firebase's hosting, storage and database.

# All(ish) libraries and assets used:
* [React](https://reactjs.org/) - ReactJS
* [Create react app](https://github.com/facebook/create-react-app) - Create react app
* [react-simple-loading](https://github.com/bbstilson/react-simple-loading) - Simple loading component
* [Google Firebase](https://firebase.google.com) - Google Firebase, used for:
    * Real time database
    * Storage
    * Hosting the [demo](https://wobservations-68289.firebaseapp.com/)
* [Pigeonmap](https://github.com/mariusandra/pigeon-maps) - Pigeon map for the map and markers
* [React Window Size](https://github.com/finnfiddle/react-window-size) - To pass the browser window's dimensions sensibly
* [Ant Design](https://ant.design/) - UI Design
* [Coolors](https://coolors.co/) - UI Colours
* [Flag icons](https://www.flaticon.com/) - Flag icons

## Demo

Live demo is available at https://wobservations-68289.firebaseapp.com/ .

## Deployment

When setting up Wobservations, make sure to have the config file in place under src/config/config.js with the following:

```javascript
// Initialize Firebase
export const DB_CONFIG = {
  apiKey: "YOUR_API_KEY_HERE",
  authDomain: "wobservations-12345.firebaseapp.com",
  databaseURL: "https://wobservations-12345.firebaseio.com",
  projectId: "wobservations-12345",
  storageBucket: "wobservations-12345.appspot.com",
  messagingSenderId: "1234567890"
};

//Mapbox access token
export const MAPBOX_ACCESS_TOKEN =
  "YOUR_MAPBOX_ACCESS_TOKEN_HERE";

```

You should be able to get all the info you need from your Firebase console and project page. The Mapbox token is available once registration has been made.

## Authors

* **Sami Lehtonen** - *Initial work* - [Samile](http://samile.fi/)

## Acknowledgments

* Thanks to R for the great challenge!
